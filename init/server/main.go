package main

import (
	"fmt"
	"net"

	"gitlab.com/foodstreets/auth/config"
	"gitlab.com/foodstreets/auth/protobuf/rpc"
	"gitlab.com/foodstreets/master/cmd"

	"gitlab.com/foodstreets/auth/api/handler"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	// declaration var
	// var name type = initValue
	auth rpc.AuthServer = handler.NewAuth()
	user rpc.UserServer = handler.NewUser()
)

func main() {
	cmd.Execute()
	conf := config.Get()
	listen, err := net.Listen("tcp", conf.GrpcAuth.Url)
	if err != nil {
		fmt.Println("Disconnection server", err)
		return
	}

	fmt.Println("Start servering...")

	opts := []grpc.ServerOption{}
	server := grpc.NewServer(opts...)
	rpc.RegisterAuthServer(server, auth)
	rpc.RegisterUserServer(server, user)
	reflection.Register(server)

	err = server.Serve(listen)
	if err != nil {
		fmt.Println("Server not listen", err)
		return
	}
}
