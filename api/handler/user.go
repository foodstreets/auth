package handler

import (
	"context"
	"strconv"

	"gitlab.com/foodstreets/auth/api/params"
	"gitlab.com/foodstreets/auth/protobuf/request"
	"gitlab.com/foodstreets/auth/protobuf/response"
	"gitlab.com/foodstreets/auth/protobuf/rpc"

	"gitlab.com/foodstreets/auth/middleware"

	"gitlab.com/foodstreets/auth/api/view"

	"gitlab.com/foodstreets/auth/api/entity"
)

type user struct{}

var _ rpc.UserServer = &user{}

func NewUser() *user {
	return &user{}
}

func (u user) Signup(ctx context.Context, req *request.SignupUser) (*response.User, error) {
	pSignUpUser := params.UserParam.ParseToSignUpUserParam(req)
	user, err := entity.UserEntity.Signup(pSignUpUser)
	if err != nil {
		return &response.User{}, err
	}

	return view.UserView.PopulateUser(user), nil
}

func (u user) Login(ctx context.Context, req *request.LoginUser) (*response.User, error) {
	pLoginUser := params.UserParam.ParseToLoginUserParam(req)
	user, err := entity.UserEntity.Login(pLoginUser)
	if err != nil {
		return &response.User{}, err
	}
	return view.UserView.PopulateUser(user), nil
}

func (u user) GetByAccessToken(ctx context.Context, req *request.AccessToken) (*response.User, error) {
	accessToken := req.AccessToken
	userId, err := middleware.MInterface.ValidateAccessToken(accessToken)
	if err != nil {
		return &response.User{}, err
	}

	userID, _ := strconv.Atoi(userId)
	user, err := entity.UserEntity.GetByID(userID)
	if err != nil {
		return &response.User{}, err
	}

	return view.UserView.PopulateUser(user), nil
}

func (u user) GetByID(ctx context.Context, req *request.User) (*response.User, error) {
	id := int(req.Id)
	user, err := entity.UserEntity.GetByID(id)
	if err != nil {
		return &response.User{}, err
	}
	return view.UserView.PopulateUser(user), nil
}

func (u user) Verify(ctx context.Context, req *request.VerifyUser) (*response.User, error) {
	user, err := entity.UserEntity.Verify(req.PhoneNumber, req.Session)
	if err != nil {
		return &response.User{}, err
	}

	return view.UserView.PopulateUser(user), nil
}

func (u user) SignupFacebook(ctx context.Context, req *request.ThirdParty) (*response.Login, error) {
	pThirdParty := params.UserParam.ParseToThirthParty(req)
	_, token, err := entity.UserEntity.SignupFacebook(pThirdParty)
	if err != nil {
		return &response.Login{}, err
	}

	return &response.Login{
		AccessToken: token,
	}, nil
}

func (u user) LoginFacebook(ctx context.Context, req *request.ThirdParty) (*response.AccountInfo, error) {
	accessToken := req.AccessToken
	facebookUser, err := entity.UserEntity.LoginFacebook(accessToken)
	if err != nil {
		return &response.AccountInfo{}, err
	}

	return &response.AccountInfo{
		Username: facebookUser.Name,
		Avatar:   "https://graph.facebook.com/" + facebookUser.Uid + "/picture?width=80&height=80",
	}, nil
}

func (u user) SignupApple(ctx context.Context, req *request.ThirdParty) (*response.Login, error) {
	pThirdParty := params.UserParam.ParseToThirthParty(req)
	_, token, err := entity.UserEntity.SignupApple(pThirdParty)
	if err != nil {
		return &response.Login{}, err
	}

	return &response.Login{
		AccessToken: token,
	}, nil
}

func (u user) SignupGoogle(ctx context.Context, req *request.ThirdParty) (*response.Login, error) {
	accessToken := req.AccessToken
	pThirdParty := params.UserParam.ParseToThirthParty(req)
	_, token, err := entity.UserEntity.SignupGoogle(accessToken, pThirdParty)
	if err != nil {
		return &response.Login{}, err
	}

	return &response.Login{
		AccessToken: token,
	}, nil
}
