package view

import (
	"fmt"
	"time"

	"gitlab.com/foodstreets/auth/protobuf/response"

	repoMaster "gitlab.com/foodstreets/master/repo"

	redisMaster "gitlab.com/foodstreets/master/redis"

	"gitlab.com/foodstreets/master/model"
)

type user struct{}

var UserView user

const (
	expirationUser = 30 * 24 * time.Hour
)

func (u user) PopulateUser(user *model.User) *response.User {
	if user == nil {
		return &response.User{}
	}

	var srcUser redisMaster.User
	key := fmt.Sprintf("user_%d", user.Id)
	err := repoMaster.Redis.Get(key, srcUser)
	if err != nil {
		return &response.User{}
	}
	if srcUser.Id == 0 {
		redisUser := redisMaster.ParseToUser(user)
		err = repoMaster.Redis.Set(key, redisUser, expirationUser)
		if err != nil {
			return &response.User{}
		}

		//Copy user to redis
		srcUser = redisUser
	}

	go repoMaster.Redis.Refresh(key, expirationUser)

	return &response.User{
		Id:                  int64(srcUser.Id),
		Username:            srcUser.Username,
		Avatar:              srcUser.Avatar,
		Last:                srcUser.Last,
		Middle:              srcUser.Middle,
		First:               srcUser.First,
		Gender:              srcUser.Gender,
		Email:               srcUser.Email,
		PhoneNumber:         srcUser.PhoneNumber,
		Birthday:            srcUser.Birthday,
		Status:              srcUser.Status,
		CityId:              int64(srcUser.CityId),
		Active:              srcUser.Active,
		Device:              srcUser.Device,
		Platform:            srcUser.Platform,
		VerifiedPhoneNumber: srcUser.VerifiedPhoneNumber,
		Session:             srcUser.Session,
	}
}
