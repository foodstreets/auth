package middleware

import (
	"context"
	"net/http"

	"gitlab.com/foodstreets/auth/api/params"

	"gitlab.com/foodstreets/master/model"
)

type middlewareAuth struct{}

var MiddlewareAuth IMiddlewareAuth

type IMiddlewareAuth interface {
	Authentication(param params.LoginUserParam, user *model.User) bool
	GenerateAccessToken(user *model.User) (string, error)
	ValidateAccessToken(next http.Handler) http.Handler
	GenerateRefreshToken(user *model.User) (string, error)
	GenerateCustomKey(userId int, tokenHash string) string
}

func (m middlewareAuth) ValidateAccessToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		token, err := extractToken(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		userID, err := MInterface.ValidateAccessToken(token)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(r.Context(), UserIDKey{}, userID)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}
