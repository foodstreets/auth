package params

import (
	"gitlab.com/foodstreets/auth/protobuf/request"
)

type SignUpUserParam struct {
	PhoneNumber string
	Password    string
	Platform    string
	Device      string
	Email       string
}

type LoginUserParam struct {
	PhoneNumber string
	Password    string
}

type VerfiyParam struct {
	PhoneNumber string
	HashToken   string
}

type ThirdPartyParam struct {
	AccessToken string
	Device      string
	Platform    string
	Email       string
	Username    string
	SDKType     string
}

type user struct{}

var UserParam user

func (user) ParseToSignUpUserParam(req *request.SignupUser) SignUpUserParam {
	return SignUpUserParam{
		PhoneNumber: req.PhoneNumber,
		Password:    req.Password,
		Platform:    req.Platform,
		Device:      req.Device,
		Email:       req.Email,
	}
}

func (user) ParseToLoginUserParam(req *request.LoginUser) LoginUserParam {
	return LoginUserParam{
		PhoneNumber: req.PhoneNumber,
		Password:    req.Password,
	}
}

func (user) ParseToThirthParty(req *request.ThirdParty) ThirdPartyParam {
	return ThirdPartyParam{
		AccessToken: req.AccessToken,
		Device:      req.Device,
		Platform:    req.Platform,
		Email:       req.Email,
		Username:    req.Username,
		SDKType:     req.SDKType,
	}
}
