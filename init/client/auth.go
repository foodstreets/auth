package client

import (
	"context"
	"time"

	"gitlab.com/foodstreets/auth/protobuf/request"
	"gitlab.com/foodstreets/auth/protobuf/response"
)

type auth struct{}

var AuthClient auth

func (auth) Authentication(req *request.Auth) (*response.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	res, err := client.Auth().Authentication(ctx, req)
	return res, err
}
