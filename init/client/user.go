package client

import (
	"context"
	"log"
	"time"

	"gitlab.com/foodstreets/auth/protobuf/request"
	"gitlab.com/foodstreets/auth/protobuf/response"

	"github.com/cenkalti/backoff"
)

type user struct{}

var UserClient IUser

type IUser interface {
	Signup(req *request.SignupUser) (*response.User, error)
	Login(req *request.LoginUser) (*response.User, error)
	GetByAccessToken(req *request.AccessToken) (*response.User, error)
	GetByID(req *request.User) (*response.User, error)
	Verify(req *request.VerifyUser) (*response.User, error)
	SignupFacebook(req *request.ThirdParty) (*response.Login, error)
	LoginFacebook(req *request.ThirdParty) (*response.AccountInfo, error)
	SignupApple(req *request.ThirdParty) (*response.Login, error)
	SignupGoogle(req *request.ThirdParty) (*response.Login, error)
}

func init() {
	UserClient = &user{}
}

func (user) Signup(req *request.SignupUser) (res *response.User, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.User().Signup(ctx, req)
		return err
	}

	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return res, err
}

func (user) Login(req *request.LoginUser) (res *response.User, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.User().Login(ctx, req)
		return err
	}

	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return res, err
}

func (user) GetByAccessToken(req *request.AccessToken) (res *response.User, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.User().GetByAccessToken(ctx, req)
		return err
	}

	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return res, err
}

func (user) GetByID(req *request.User) (res *response.User, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.User().GetByID(ctx, req)
		return err
	}

	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return res, err
}

func (user) Verify(req *request.VerifyUser) (res *response.User, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.User().Verify(ctx, req)
		return err
	}

	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return res, err
}

func (user) SignupFacebook(req *request.ThirdParty) (res *response.Login, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.User().SignupFacebook(ctx, req)
		return err
	}

	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return res, err
}

func (user) LoginFacebook(req *request.ThirdParty) (res *response.AccountInfo, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.User().LoginFacebook(ctx, req)
		return err
	}

	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return res, err
}

func (user) SignupApple(req *request.ThirdParty) (res *response.Login, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.User().SignupApple(ctx, req)
		return err
	}

	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return res, err
}

func (user) SignupGoogle(req *request.ThirdParty) (res *response.Login, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.User().SignupGoogle(ctx, req)
		return err
	}

	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return res, err
}
