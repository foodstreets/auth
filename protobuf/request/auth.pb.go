// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/request/auth.proto

/*
Package request is a generated protocol buffer package.

It is generated from these files:
	proto/request/auth.proto
	proto/request/user.proto

It has these top-level messages:
	Auth
	User
	SignupUser
	LoginUser
	AccessToken
	VerifyUser
	ThirdParty
*/
package request

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Auth struct {
	AccessToken string `protobuf:"bytes,1,opt,name=AccessToken" json:"AccessToken,omitempty"`
}

func (m *Auth) Reset()                    { *m = Auth{} }
func (m *Auth) String() string            { return proto.CompactTextString(m) }
func (*Auth) ProtoMessage()               {}
func (*Auth) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *Auth) GetAccessToken() string {
	if m != nil {
		return m.AccessToken
	}
	return ""
}

func init() {
	proto.RegisterType((*Auth)(nil), "request.auth.Auth")
}

func init() { proto.RegisterFile("proto/request/auth.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 127 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x28, 0x28, 0xca, 0x2f,
	0xc9, 0xd7, 0x2f, 0x4a, 0x2d, 0x2c, 0x4d, 0x2d, 0x2e, 0xd1, 0x4f, 0x2c, 0x2d, 0xc9, 0xd0, 0x03,
	0x0b, 0x09, 0xf1, 0x40, 0xc5, 0xf4, 0x40, 0x62, 0x4a, 0x1a, 0x5c, 0x2c, 0x8e, 0xa5, 0x25, 0x19,
	0x42, 0x0a, 0x5c, 0xdc, 0x8e, 0xc9, 0xc9, 0xa9, 0xc5, 0xc5, 0x21, 0xf9, 0xd9, 0xa9, 0x79, 0x12,
	0x8c, 0x0a, 0x8c, 0x1a, 0x9c, 0x41, 0xc8, 0x42, 0x4e, 0x7a, 0x51, 0x3a, 0xe9, 0x99, 0x25, 0x39,
	0x89, 0x49, 0x7a, 0xc9, 0xf9, 0xb9, 0xfa, 0x69, 0xf9, 0xf9, 0x29, 0xc5, 0x25, 0x45, 0xa9, 0xa9,
	0x25, 0xc5, 0x60, 0xc3, 0xf5, 0xc1, 0x86, 0x27, 0x95, 0xa6, 0xc1, 0xac, 0x4c, 0x62, 0x03, 0x8b,
	0x18, 0x03, 0x02, 0x00, 0x00, 0xff, 0xff, 0x65, 0x0a, 0xc9, 0x4c, 0x8a, 0x00, 0x00, 0x00,
}
