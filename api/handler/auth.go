package handler

import (
	"context"

	"gitlab.com/foodstreets/auth/api/view"
	"gitlab.com/foodstreets/auth/protobuf/request"
	"gitlab.com/foodstreets/auth/protobuf/response"
	"gitlab.com/foodstreets/auth/protobuf/rpc"

	"gitlab.com/foodstreets/auth/api/entity"
)

type auth struct{}

var _ rpc.AuthServer = &auth{}

func NewAuth() *auth {
	return &auth{}
}

func (a auth) Authentication(ctx context.Context, req *request.Auth) (*response.User, error) {
	accessToken := req.AccessToken
	auth, err := entity.AuthEntity.Authentication(accessToken)
	if err != nil {
		return nil, err
	}

	resp := view.UserView.PopulateUser(auth)
	return resp, nil
}
