package client

import (
	"fmt"
	"time"

	"gitlab.com/foodstreets/auth/protobuf/rpc"

	"github.com/cenkalti/backoff"
	"google.golang.org/grpc"
)

var (
	client *Client
)

const (
	InitialInterval = 300 * time.Millisecond
	MaxElapsedTime  = 3 * time.Minute
)

type Client struct {
	auth rpc.AuthClient
	user rpc.UserClient
}

type GrpcConfig struct {
	Address string
}

func InitClient(conf GrpcConfig) {
	conn, err := grpc.Dial(conf.Address, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	fmt.Println("Auth PONG")

	client = &Client{
		auth: rpc.NewAuthClient(conn),
		user: rpc.NewUserClient(conn),
	}
}

func (c Client) Auth() rpc.AuthClient {
	return c.auth
}

func (c Client) User() rpc.UserClient {
	return c.user
}

func grpcClient() *Client {
	if client == nil {
		panic("Disconect")
	}
	return client
}

func getExponentialBackOff() *backoff.ExponentialBackOff {
	b := backoff.NewExponentialBackOff()
	b.InitialInterval = InitialInterval
	b.MaxElapsedTime = MaxElapsedTime
	return b
}
