package entity

import (
	"errors"
	"fmt"

	"gitlab.com/foodstreets/auth/utils"

	"gitlab.com/foodstreets/auth/api/params"

	mMiddleware "gitlab.com/foodstreets/master/middleware"

	mUtils "gitlab.com/foodstreets/master/utils"

	AuthMidd "gitlab.com/foodstreets/auth/middleware"

	repoMaster "gitlab.com/foodstreets/master/repo"

	"gitlab.com/foodstreets/master/orm"

	"gitlab.com/foodstreets/master/model"

	"github.com/gin-gonic/gin"
)

type user struct{}

var UserEntity IUser

type IUser interface {
	Signup(param params.SignUpUserParam) (*model.User, error)
	Login(param params.LoginUserParam) (*model.User, error)
	GetByID(ID int) (*model.User, error)
	Verify(phoneNumber, session string) (*model.User, error)
	SignupFacebook(param params.ThirdPartyParam) (*model.User, string, error)
	LoginFacebook(accessToken string) (*model.Facebook, error)
	SignupApple(param params.ThirdPartyParam) (*model.User, string, error)
	SignupGoogle(accessToken string, param params.ThirdPartyParam) (*model.User, string, error)
}

func init() {
	UserEntity = &user{}
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func (u user) Signup(param params.SignUpUserParam) (*model.User, error) {
	resp := &model.User{}
	user, err := orm.User.GetByPhoneEmail(param.PhoneNumber, param.Email)
	if err != nil {
		return resp, err
	}
	if user != nil {
		err := errors.New(fmt.Sprintf("User exists by phoneNumber: %s, email: %s", param.PhoneNumber, param.Email))
		return resp, err
	}

	hashPass, err := utils.HashPassword(param.Password)
	if err != nil {
		return resp, nil
	}

	token := mUtils.GenerateRandomString(15, letterBytes)
	mUser := &model.User{
		PhoneNumber: param.PhoneNumber,
		Password:    hashPass,
		TokenHash:   token,
		Platform:    param.Platform,
		Device:      param.Device,
	}

	err = orm.User.Create(mUser)
	return mUser, err
}

func (u user) Login(param params.LoginUserParam) (*model.User, error) {
	resp := &model.User{}
	user, err := orm.User.GetByPhoneNumber(param.PhoneNumber)
	if err != nil {
		return resp, errors.New(fmt.Sprintf("Can't get use by phoneNumber", err))
	}
	if user == nil {
		return nil, nil
	}

	if valid := AuthMidd.MInterface.Authentication(param, user); !valid {
		return resp, errors.New(fmt.Sprintf("Authentication failed", err))
	}

	accessToken, err := AuthMidd.MInterface.GenerateAccessToken(user)
	if err != nil {
		return resp, errors.New(fmt.Sprintf("GenerateAccessToken failed", err))
	}

	//	refreshToken, err := AuthMidd.MInterface.GenerateRefreshToken(user)
	//	if err != nil {
	//		return resp, errors.New(fmt.Sprintf("RefreshToken failed", err))
	//	}
	resp = &model.User{
		TokenHash: accessToken,
	}

	return resp, nil
}

func (u user) GetByID(ID int) (*model.User, error) {
	resp := &model.User{}
	user, err := orm.User.GetByID(ID)
	if err != nil {
		return resp, nil
	}

	return &model.User{
		Id: user.Id,
	}, nil
}

func (u user) Verify(phoneNumber, session string) (*model.User, error) {
	resp := &model.User{}
	user, err := orm.User.GetByPhoneNumber(phoneNumber)
	if err != nil {
		return resp, nil
	}
	if user == nil {
		err := errors.New(fmt.Sprintf("User not exist by phoneNumber: %s", phoneNumber))
		return resp, err
	}

	//verify session
	modelUser := &model.User{}
	key := fmt.Sprintf("user_%d", user.Id)
	err = repoMaster.Redis.Get(key, modelUser)
	if err != nil {
		return resp, err
	}
	if modelUser == nil {
		err := errors.New(fmt.Sprintf("User not exist redis by phoneNumber: %s", phoneNumber))
		return resp, err
	}
	if modelUser.TokenHash != session {
		return resp, errors.New(fmt.Sprintf("Session invalid. Please check session"))
	}

	// Active user
	user.Active = true
	err = orm.User.Update(user)
	return user, err
}

func (u user) SignupFacebook(param params.ThirdPartyParam) (*model.User, string, error) {
	user := &model.User{}
	facebookUser, err := repoMaster.Facebook.GetUserByAccessToken(param.AccessToken)
	if err != nil {
		return user, "", err
	}
	if facebookUser == nil {
		return user, "", nil
	}

	userAccount, err := orm.UserAccount.GetByUID(facebookUser.Uid)
	if err != nil {
		return user, "", err
	}
	if userAccount != nil {
		return user, "", nil
	}

	userAccount.AccessToken = param.AccessToken
	respUser, token, err := u.updateSession(userAccount, param)
	return respUser, token, err
}

func (u user) updateSession(userAccount *model.UserAccount, param params.ThirdPartyParam) (user *model.User, token string, err error) {
	err = orm.UserAccount.Update(userAccount)
	if err != nil {
		return
	}

	c := gin.Context{}
	middleware := mMiddleware.SecureCookie{}
	token, err = middleware.SetAuthentication("token", userAccount.UserId, "/", c.Writer)
	if err != nil {
		return
	}

	user, err = orm.User.GetByID(userAccount.UserId)
	if err != nil {
		return
	}

	if len(param.Platform) > 0 {
		user.Platform = param.Platform
	}
	if len(param.Device) > 0 {
		user.Device = param.Device
	}

	err = orm.User.Create(user)
	return
}

func (u user) LoginFacebook(accessToken string) (*model.Facebook, error) {
	facebookUser, err := repoMaster.Facebook.GetUserByAccessToken(accessToken)
	return facebookUser, err
}

func (u user) SignupApple(param params.ThirdPartyParam) (*model.User, string, error) {
	user := &model.User{}
	appleUser, err := repoMaster.Apple.GetUserByAccessToken(param.AccessToken, param.SDKType)
	if err != nil {
		return user, "", err
	}

	userAccount, err := orm.UserAccount.GetByUID(appleUser.Uid)
	if err != nil {
		return user, "", err
	}
	if userAccount != nil {
		return user, "", nil
	}

	userAccount.AccessToken = userAccount.AccessToken
	userAccount.RefreshToken = userAccount.RefreshToken
	userAccount.TokenExpiredAt = userAccount.TokenExpiredAt
	user, token, err := u.updateSession(userAccount, param)
	return user, token, err
}

func (u user) SignupGoogle(accessToken string, param params.ThirdPartyParam) (*model.User, string, error) {
	user := &model.User{}
	ggUser, err := repoMaster.Google.GetUserByAccessToken(accessToken)
	if err != nil {
		return user, "", err
	}
	if ggUser == nil {
		return user, "", nil
	}

	userAccount, err := orm.UserAccount.GetByUID(ggUser.Id)
	if err != nil {
		return user, "", err
	}
	if userAccount != nil {
		return user, "", nil
	}

	userAccount.AccessToken = param.AccessToken
	user, token, err := u.updateSession(userAccount, param)
	return user, token, err
}
