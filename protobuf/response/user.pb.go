// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/response/user.proto

package response

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type User struct {
	Id                  int64  `protobuf:"varint,1,opt,name=Id" json:"Id,omitempty"`
	PhoneNumber         string `protobuf:"bytes,2,opt,name=PhoneNumber" json:"PhoneNumber,omitempty"`
	TokenHash           string `protobuf:"bytes,3,opt,name=TokenHash" json:"TokenHash,omitempty"`
	CreatedAt           int64  `protobuf:"varint,4,opt,name=CreatedAt" json:"CreatedAt,omitempty"`
	UpdatedAt           int64  `protobuf:"varint,5,opt,name=UpdatedAt" json:"UpdatedAt,omitempty"`
	CreatedBy           int64  `protobuf:"varint,6,opt,name=CreatedBy" json:"CreatedBy,omitempty"`
	UpdatedBy           int64  `protobuf:"varint,7,opt,name=UpdatedBy" json:"UpdatedBy,omitempty"`
	Username            string `protobuf:"bytes,8,opt,name=Username" json:"Username,omitempty"`
	Avatar              string `protobuf:"bytes,9,opt,name=Avatar" json:"Avatar,omitempty"`
	Last                string `protobuf:"bytes,10,opt,name=Last" json:"Last,omitempty"`
	Middle              string `protobuf:"bytes,11,opt,name=Middle" json:"Middle,omitempty"`
	First               string `protobuf:"bytes,12,opt,name=First" json:"First,omitempty"`
	Device              string `protobuf:"bytes,13,opt,name=Device" json:"Device,omitempty"`
	Birthday            string `protobuf:"bytes,14,opt,name=Birthday" json:"Birthday,omitempty"`
	Status              string `protobuf:"bytes,15,opt,name=Status" json:"Status,omitempty"`
	Active              bool   `protobuf:"varint,16,opt,name=Active" json:"Active,omitempty"`
	Platform            string `protobuf:"bytes,17,opt,name=Platform" json:"Platform,omitempty"`
	VerifiedPhoneNumber bool   `protobuf:"varint,18,opt,name=VerifiedPhoneNumber" json:"VerifiedPhoneNumber,omitempty"`
	Gender              string `protobuf:"bytes,19,opt,name=Gender" json:"Gender,omitempty"`
	Email               string `protobuf:"bytes,20,opt,name=Email" json:"Email,omitempty"`
	CityId              int64  `protobuf:"varint,21,opt,name=CityId" json:"CityId,omitempty"`
	Session             string `protobuf:"bytes,22,opt,name=Session" json:"Session,omitempty"`
}

func (m *User) Reset()                    { *m = User{} }
func (m *User) String() string            { return proto.CompactTextString(m) }
func (*User) ProtoMessage()               {}
func (*User) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{0} }

func (m *User) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *User) GetPhoneNumber() string {
	if m != nil {
		return m.PhoneNumber
	}
	return ""
}

func (m *User) GetTokenHash() string {
	if m != nil {
		return m.TokenHash
	}
	return ""
}

func (m *User) GetCreatedAt() int64 {
	if m != nil {
		return m.CreatedAt
	}
	return 0
}

func (m *User) GetUpdatedAt() int64 {
	if m != nil {
		return m.UpdatedAt
	}
	return 0
}

func (m *User) GetCreatedBy() int64 {
	if m != nil {
		return m.CreatedBy
	}
	return 0
}

func (m *User) GetUpdatedBy() int64 {
	if m != nil {
		return m.UpdatedBy
	}
	return 0
}

func (m *User) GetUsername() string {
	if m != nil {
		return m.Username
	}
	return ""
}

func (m *User) GetAvatar() string {
	if m != nil {
		return m.Avatar
	}
	return ""
}

func (m *User) GetLast() string {
	if m != nil {
		return m.Last
	}
	return ""
}

func (m *User) GetMiddle() string {
	if m != nil {
		return m.Middle
	}
	return ""
}

func (m *User) GetFirst() string {
	if m != nil {
		return m.First
	}
	return ""
}

func (m *User) GetDevice() string {
	if m != nil {
		return m.Device
	}
	return ""
}

func (m *User) GetBirthday() string {
	if m != nil {
		return m.Birthday
	}
	return ""
}

func (m *User) GetStatus() string {
	if m != nil {
		return m.Status
	}
	return ""
}

func (m *User) GetActive() bool {
	if m != nil {
		return m.Active
	}
	return false
}

func (m *User) GetPlatform() string {
	if m != nil {
		return m.Platform
	}
	return ""
}

func (m *User) GetVerifiedPhoneNumber() bool {
	if m != nil {
		return m.VerifiedPhoneNumber
	}
	return false
}

func (m *User) GetGender() string {
	if m != nil {
		return m.Gender
	}
	return ""
}

func (m *User) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *User) GetCityId() int64 {
	if m != nil {
		return m.CityId
	}
	return 0
}

func (m *User) GetSession() string {
	if m != nil {
		return m.Session
	}
	return ""
}

func init() {
	proto.RegisterType((*User)(nil), "response.user.User")
}

func init() { proto.RegisterFile("proto/response/user.proto", fileDescriptor1) }

var fileDescriptor1 = []byte{
	// 400 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0x92, 0xdd, 0x6e, 0xd3, 0x30,
	0x14, 0xc7, 0x95, 0x2e, 0xeb, 0x5a, 0x8f, 0x0d, 0xf0, 0xc6, 0x74, 0x40, 0x5c, 0x44, 0x5c, 0xf5,
	0x86, 0x06, 0x89, 0x27, 0x58, 0xc6, 0x57, 0x25, 0x40, 0x53, 0xc7, 0xb8, 0xe0, 0xce, 0xa9, 0x4f,
	0x88, 0x45, 0x12, 0x57, 0xf6, 0x49, 0xa5, 0xbc, 0x28, 0xcf, 0x83, 0x6c, 0x27, 0x6d, 0x2a, 0x71,
	0xd7, 0xdf, 0xff, 0xe3, 0xf8, 0xb8, 0x31, 0x7b, 0xb9, 0x35, 0x9a, 0x74, 0x6a, 0xd0, 0x6e, 0x75,
	0x63, 0x31, 0x6d, 0x2d, 0x9a, 0xa5, 0xd7, 0xf8, 0xc5, 0x20, 0x2e, 0x9d, 0xf8, 0xe6, 0x6f, 0xcc,
	0xe2, 0x47, 0x8b, 0x86, 0x5f, 0xb2, 0xc9, 0x4a, 0x42, 0x94, 0x44, 0x8b, 0x93, 0xf5, 0x64, 0x25,
	0x79, 0xc2, 0xce, 0xef, 0x4b, 0xdd, 0xe0, 0xf7, 0xb6, 0xce, 0xd1, 0xc0, 0x24, 0x89, 0x16, 0xf3,
	0xf5, 0x58, 0xe2, 0xaf, 0xd9, 0xfc, 0x87, 0xfe, 0x83, 0xcd, 0x17, 0x61, 0x4b, 0x38, 0xf1, 0xfe,
	0x41, 0x70, 0xee, 0x9d, 0x41, 0x41, 0x28, 0x6f, 0x09, 0x62, 0x3f, 0xf6, 0x20, 0x38, 0xf7, 0x71,
	0x2b, 0x7b, 0xf7, 0x34, 0xb8, 0x7b, 0x61, 0xd4, 0xcd, 0x3a, 0x98, 0x1e, 0x75, 0xb3, 0x6e, 0xd4,
	0xcd, 0x3a, 0x38, 0x3b, 0xea, 0x66, 0x1d, 0x7f, 0xc5, 0x66, 0xee, 0x3e, 0x8d, 0xa8, 0x11, 0x66,
	0x7e, 0xa9, 0x3d, 0xf3, 0x1b, 0x36, 0xbd, 0xdd, 0x09, 0x12, 0x06, 0xe6, 0xde, 0xe9, 0x89, 0x73,
	0x16, 0x7f, 0x15, 0x96, 0x80, 0x79, 0xd5, 0xff, 0x76, 0xd9, 0x6f, 0x4a, 0xca, 0x0a, 0xe1, 0x3c,
	0x64, 0x03, 0xf1, 0x6b, 0x76, 0xfa, 0x49, 0x19, 0x4b, 0xf0, 0xc4, 0xcb, 0x01, 0x5c, 0xfa, 0x03,
	0xee, 0xd4, 0x06, 0xe1, 0x22, 0xa4, 0x03, 0xb9, 0x6d, 0x32, 0x65, 0xa8, 0x94, 0xa2, 0x83, 0xcb,
	0xb0, 0xcd, 0xc0, 0xae, 0xf3, 0x40, 0x82, 0x5a, 0x0b, 0x4f, 0x43, 0x27, 0x90, 0xdf, 0x72, 0x43,
	0x6a, 0x87, 0xf0, 0x2c, 0x89, 0x16, 0xb3, 0x75, 0x4f, 0x6e, 0xd6, 0x7d, 0x25, 0xa8, 0xd0, 0xa6,
	0x86, 0xe7, 0x61, 0xd6, 0xc0, 0xfc, 0x1d, 0xbb, 0xfa, 0x89, 0x46, 0x15, 0x0a, 0xe5, 0xf8, 0xab,
	0x71, 0x3f, 0xe0, 0x7f, 0x96, 0x3b, 0xe5, 0x33, 0x36, 0x12, 0x0d, 0x5c, 0x85, 0xd3, 0x03, 0xb9,
	0xfb, 0x7d, 0xac, 0x85, 0xaa, 0xe0, 0x3a, 0xdc, 0xcf, 0x83, 0x4b, 0xdf, 0x29, 0xea, 0x56, 0x12,
	0x5e, 0xf8, 0x3f, 0xbc, 0x27, 0x0e, 0xec, 0xec, 0x01, 0xad, 0x55, 0xba, 0x81, 0x1b, 0x9f, 0x1f,
	0x30, 0x4b, 0x7f, 0xbd, 0xfd, 0xad, 0xa8, 0x12, 0xf9, 0x72, 0xa3, 0xeb, 0xb4, 0xd0, 0x5a, 0x5a,
	0x32, 0x88, 0x64, 0x53, 0xd1, 0x52, 0x99, 0xfa, 0xc7, 0x98, 0xb7, 0xc5, 0xfe, 0x8d, 0xe6, 0x53,
	0x2f, 0xbd, 0xff, 0x17, 0x00, 0x00, 0xff, 0xff, 0x08, 0x2b, 0x29, 0x89, 0xbc, 0x02, 0x00, 0x00,
}
