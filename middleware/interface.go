package middleware

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	authConf "gitlab.com/foodstreets/auth/config"

	"gitlab.com/foodstreets/auth/api/params"

	"gitlab.com/foodstreets/master/model"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

type UserIDKey struct{}

type Config struct {
	config *authConf.Config
}

type AccessTokenCustomClaims struct {
	UserID  string
	KeyType string
	jwt.StandardClaims
}

type RefreshTokenCustomClaims struct {
	UserID    string
	CustomKey string
	KeyType   string
	jwt.StandardClaims
}

type Interface interface {
	Authentication(param params.LoginUserParam, user *model.User) bool
	GenerateAccessToken(user *model.User) (string, error)
	GenerateRefreshToken(user *model.User) (string, error)
	GenerateCustomKey(userID int, tokenHash string) string
	ValidateAccessToken(tokenString string) (string, error)
	ValidateRefreshToken(tokenString string) (string, string, error)
}

type mInterface struct{}

var MInterface mInterface

func Init() *Config {
	return &Config{
		config: authConf.InitConfig(),
	}
}

func (m mInterface) ValidateAccessToken(tokenString string) (string, error) {
	token, err := jwt.ParseWithClaims(tokenString, &AccessTokenCustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New("Unexpect signing method in authtication")
		}

		configs := &Config{}

		verifyBytes, err := ioutil.ReadFile(configs.config.AccessTokenPublicKeyPath)
		if err != nil {
			return nil, err
		}

		verifyKey, err := jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
		if err != nil {
			return nil, err
		}

		return verifyKey, nil
	})

	if err != nil {
		return "", errors.New(fmt.Sprintf("Unable parse claimn", err))
	}

	claims, ok := token.Claims.(*AccessTokenCustomClaims)
	if !ok || !token.Valid || claims.UserID == "" || claims.KeyType != "access" {
		return "", errors.New("Invalid token: Authentication failed")
	}

	return claims.UserID, nil
}

func (m mInterface) ValidateRefreshToken(tokenString string) (string, string, error) {
	token, err := jwt.ParseWithClaims(tokenString, &RefreshTokenCustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New("Unexpect signing method in authtication")
		}

		configs := &Config{}
		verifyBytes, err := ioutil.ReadFile(configs.config.RefreshTokenPublicKeyPath)
		if err != nil {
			return nil, err
		}

		verifyKey, err := jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
		if err != nil {
			return nil, err
		}

		return verifyKey, nil
	})

	if err != nil {
		return "", "", errors.New(fmt.Sprintf("Unable parse claimn", err))
	}

	claims, ok := token.Claims.(*RefreshTokenCustomClaims)
	if !ok || !token.Valid || claims.UserID == "" || claims.KeyType != "access" {
		return "", "", errors.New("Invalid token: Authentication failed")
	}

	return claims.UserID, claims.CustomKey, nil
}

func (m mInterface) Authentication(param params.LoginUserParam, user *model.User) (b bool) {
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(param.Password)); err != nil {
		return
	}

	return true
}

func (m mInterface) GenerateAccessToken(user *model.User) (accessToken string, err error) {
	userID := user.Id
	tokenType := "access"

	configs := authConf.InitConfig()
	claims := AccessTokenCustomClaims{
		string(userID),
		tokenType,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * time.Duration(configs.JwtExpiration)).Unix(),
			Issuer:    "user.auth.service",
		},
	}

	signBytes, err := ioutil.ReadFile(configs.AccessTokenPrivateKeyPath)
	if err != nil {
		return "", errors.New(fmt.Sprintf("Unable readfile file accessToken privateKey"))
	}

	signKey, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		return "", errors.New(fmt.Sprintf("Unable signKey accessToken privateKey"))
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	return token.SignedString(signKey)

}

func (m mInterface) GenerateRefreshToken(user *model.User) (refreshToken string, err error) {
	customKey := m.GenerateCustomKey(user.Id, user.TokenHash)
	tokenType := "refresh"

	configs := &Config{}
	claims := RefreshTokenCustomClaims{
		string(user.Id),
		customKey,
		tokenType,
		jwt.StandardClaims{
			Issuer: "user.auth.service",
		},
	}

	signBytes, err := ioutil.ReadFile(configs.config.RefreshTokenPrivateKeyPath)
	if err != nil {
		return "", errors.New(fmt.Sprintf("Unable readfile file refreshToken privateKey"))
	}

	signKey, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		return "", errors.New(fmt.Sprintf("Unable signKey refreshToken privateKey"))
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	return token.SignedString(signKey)

	return
}

func (m mInterface) GenerateCustomKey(userID int, tokenHash string) string {
	h := hmac.New(sha256.New, []byte(tokenHash))
	h.Write([]byte(string(userID)))
	sha := hex.EncodeToString(h.Sum(nil))
	return sha
}

func extractToken(r *http.Request) (string, error) {
	header := r.Header.Get("Authorization")
	content := strings.Split(header, " ")
	if len(content) != 2 {
		return "", errors.New("Token not provied or malformed")
	}
	return content[1], nil
}
