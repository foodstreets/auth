package config

import (
	"sync"

	"gitlab.com/foodstreets/master/config"

	"github.com/spf13/viper"
)

var (
	conf Config
	once sync.Once
)

type Config struct {
	config.Config
	AccessTokenPrivateKeyPath  string
	AccessTokenPublicKeyPath   string
	RefreshTokenPrivateKeyPath string
	RefreshTokenPublicKeyPath  string
	JwtExpiration              int // minute
}

func InitConfig() *Config {
	viper.AutomaticEnv()
	viper.SetDefault("ACCESS_TOKEN_PRIVATE_KEY_PATH", "./pem/token-private.pem")
	viper.SetDefault("ACCESS_TOKEN_PUBLIC_KEY_PATH", "./pem/token-public.pem")
	viper.SetDefault("REFRESH_TOKEN_PRIVATE_KEY_PATH", "./pem/refresh-token-private.pem")
	viper.SetDefault("REFRESH_TOKEN_PUBLIC_KEY_PATH", "./pem/refresh-token-public.pem")
	viper.SetDefault("JWT_EXPIRATION", 30)

	config := &Config{
		AccessTokenPrivateKeyPath:  viper.GetString("ACCESS_TOKEN_PRIVATE_KEY_PATH"),
		AccessTokenPublicKeyPath:   viper.GetString("ACCESS_TOKEN_PUBLIC_KEY_PATH"),
		RefreshTokenPrivateKeyPath: viper.GetString("REFRESH_TOKEN_PRIVATE_KEY_PATH"),
		RefreshTokenPublicKeyPath:  viper.GetString("REFRESH_TOKEN_PUBLIC_KEY_PATH"),
		JwtExpiration:              viper.GetInt("JWT_EXPIRATION"),
	}

	return config
}

func load() {
	once.Do(func() {
		conf.Config = config.Load()
	})
}

func Load() Config {
	load()
	return conf
}

func Get() Config {
	load()
	return conf
}
